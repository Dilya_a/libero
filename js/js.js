// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')


var accordion   = $(".accordion"),
	datepicker  = $(".datepicker_ui");


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}



$(document).ready(function () {

	$(".menu_btn, .menu_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    })

    $(".file__attach").change(function(){
		var filename = $(this).val().replace(/.*\\/, "");
		$(".checks__file-text").text(filename);
	});

    $(".file__attach1").change(function(){
		var filename = $(this).val().replace(/.*\\/, "");
		$(".checks__file-text1").text(filename);
	});



    if(datepicker.length){
    	$(datepicker).datepicker({
    		language: 'ru'
    	});
    	
    }

	// ================= Drop zone=================
	// if (dropzone.length){
	// 	dropzone.dropzone();
	// }
	
	// =================modal start=================
		// if(popup.length){
		// 	popup.on('click',function(){
		// 	    var modal = $(this).data("popup");
		// 	    $(modal).arcticmodal();
		// 	});
		// }
    // =================modal end=================


    // =================Custom form elements=================

    // if(tabs.length){
    // 	tabs.init();
    // }

    /* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

		if($('.accordion_item_link').length){
			$('.accordion_item_link').on('click', function(){
				$(this)
				.toggleClass('active')
				.next('.sub-menu')
				.slideToggle()
				.parents(".accordion_item")
				.siblings(".accordion_item")
				.find(".accordion_item_link")
				.removeClass("active")
				.next(".sub-menu")
				.slideUp();
			});  
		}
		$('.accordion_item_link').on('click', function(e) {
		    e.preventDefault();
		});


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */


});


$(window).load(function () {
	$('body').addClass('loaded');
})